# Local Sentry

### Step 1. Create `.env`

```
$ cp .env.template .env
```

### Step 2. Set secret key

Generate secret key

```
$ docker-compose run --rm sentry_web config generate-secret-key
```

Then set the output to SENTRY_SECRET_KEY variable in `.env`

### Step 3. Apply db migrations and create user

```
$ docker-compose run --rm sentry_web upgrade
```
While running this command user creation with interactive dialog will be offered.

Set your email and pass to access Sentry.

### Step 3. Run it
```
docker-compose up -d
```

### Step 4. Log in

Go to your browser at `http://127.0.0.1:<SENTRY_PORT>` and fill up the login form. 

After log in your will be offered to compete setup. In `Root URL` field set your PC ip to make Sentry available for services.

Press `Continue button`. DONE.

## TODO

1. Add email sending